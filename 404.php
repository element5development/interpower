<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main id="main-content">
	<header>
		<div>
			<h1>Hmmm…That page doesn’t exist.</h1>
			<hr>
			<p>It looks like that page doesn’t exist or we can’t find it. Contact us if you are having trouble finding something.</p>
			<a class="button" href="<?php the_permalink(419); ?>">
				Contact Us
			</a>
		</div>
	</header>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>