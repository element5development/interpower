<?php 
/*----------------------------------------------------------------*\

	ARCHIVE FOR CPT: applications

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php $image = get_field('application_archive_background', 'option'); ?>
<header class="post-header lazyload" data-expand="250" data-bgset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w" data-sizes="auto">
	<div>
		<h1><?php the_field('application_archive_title', 'option'); ?></h1>
		<p><?php the_field('application_archive_description', 'option'); ?></p>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="application-archive card-grid standard-cards is-extra-wide columns-3">
				<?php	while ( have_posts() ) : the_post(); ?>
					<a class="card" href="<?php the_permalink(); ?>">
						<div class="card">
							<?php if ( get_field('title_background') ) : ?>
								<?php $image = get_field('title_background'); ?>
								<figure>
									<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								</figure>
							<?php else : ?>
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/no-set-image.PNG" alt="<?php echo $image['alt']; ?>">
								</figure>
							<?php endif; ?>
							<h2><?php the_title(); ?></h2>
							<?php the_field('post_description'); ?>
							<div class="button">
								Learn More
							</div>
						</div>
					</a>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<section class="is-narrow">
				<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
			</section>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
		<?php clean_pagination(); ?>

		<?php if ( get_field('application_archive_cover_headline', 'option') ) : ?>
			<section class="cover <?php the_sub_field('width'); ?> lazyload blur-up">
				<div>
					<h2><?php the_field('application_archive_cover_headline', 'option'); ?></h2>
					<?php 
						$link = get_field('application_archive_cover_button', 'option'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
						<?php echo esc_html($link_title); ?>
					</a>
				</div>
			</section>
		<?php endif; ?>

	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>