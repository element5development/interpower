<?php 
/*----------------------------------------------------------------*\

	ARCHIVE FOR CPT: locations

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<header class="post-header has-no-image lazyload">
	<div>
		<h1><?php the_field('location_archive_title', 'option'); ?></h1>
		<p><?php the_field('location_archive_description', 'option'); ?></p>
		<?php $image = get_field('location_archive_image', 'option'); ?>
		<?php if ( get_field('location_archive_image', 'option') ) : ?>
			<figure>
				<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</figure>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="location-archive card-grid standard-cards is-wide columns-3">
				<?php	while ( have_posts() ) : the_post(); ?>
					<a class="card" href="<?php the_permalink(); ?>">
						<div class="card">
							<h2><?php the_title(); ?></h2>
							<div class="button">
								Learn More
							</div>
						</div>
					</a>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<section class="is-narrow">
				<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
			</section>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
		<?php clean_pagination(); ?>

		<?php get_template_part('template-parts/sections/article/newsletter'); ?>

	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>