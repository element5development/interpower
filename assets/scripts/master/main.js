var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		NOTIFICATION BAR
	\*----------------------------------------------------------------*/
	if (readCookie('siteNotification') === 'false') {
		$('.site-notification').removeClass("is-active");
	} else {
		$('.site-notification').addClass("is-active");
	}
	$('.site-notification button').click(function () {
		$('.site-notification').removeClass("is-active");
		createCookie('siteNotification', 'false');
	});

	if (readCookie('cookie-useage-notification') === 'false') {
		$('.cookie-useage-notification').removeClass("is-active");
	} else {
		$('.cookie-useage-notification').addClass("is-active");
	}
	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("is-active");
		createCookie('cookie-useage-notification', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		GALLERY IMAGE CHANGE
	\*----------------------------------------------------------------*/
	$('.gallery .images button').click(function () {
		var attr = $(this).children('img').attr('src');
		var caption = $(this).children('img').attr('alt');
		$(this).closest('.gallery').find('.featured img').attr('src', attr);
		$(this).closest('.gallery').find('.featured p').html(caption);
		// $('.gallery .featured img').attr('src', attr);
		// $('.gallery .featured p').html(caption);
	});
	/*----------------------------------------------------------------*\
		SEARCH FORM TOGGLE
	\*----------------------------------------------------------------*/
	$('button.search').on('click', function () {
		$('.primary-navigation form').toggleClass('is-open');
		$('.primary-navigation form input').focus();
	});
	$('form.search button.close').on('click', function () {
		$('.primary-navigation form').toggleClass('is-open');
	});
	/*----------------------------------------------------------------*\
		MOBILE MENU TOGGLE
	\*----------------------------------------------------------------*/
	$('button.menu').on('click', function () {
		$('html').toggleClass("cannot-scroll");
		$('.mobile-menu').toggleClass('is-open');
	});
	$(document).click(function (event) {
		//if you click on anything except within the open menu, close the menu
		if (!$(event.target).closest(".mobile-menu, button.menu").length) {
			$('html').removeClass("cannot-scroll");
			$(".mobile-menu").removeClass("is-open");
		}
	});
	/*----------------------------------------------------------------*\
		VIDEO SLIDER
	\*----------------------------------------------------------------*/
	$('.video-slider .videos').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 3000,
		responsive: [{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	/*----------------------------------------------------------------*\
		MODALS
	\*----------------------------------------------------------------*/
	MicroModal.init({
		openClass: 'is-open', // [5]
		disableScroll: true, // [6]
		disableFocus: false, // [7]
		awaitOpenAnimation: false, // [8]
		awaitCloseAnimation: false, // [9]
		debugMode: true // [10]
	});
	//disable video when lightbox closed
	$('.modal__close, .modal__overlay').on("click", function() {
		var video = $("#video-player").attr("src");
		$("#video-player").attr("src","");
		$("#video-player").attr("src",video);
	});
});