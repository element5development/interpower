<?php
/*----------------------------------------------------------------*\
	INITIALIZE BUTTON SHORTCODE
\*----------------------------------------------------------------*/
function button_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'size' => 'normal',
			'color' => 'black',
			'target' => '_self',
			'url' => '#',
		),
		$atts,
		'button'
	);
	$size = $atts['size'];
	$color = $atts['color'];
	$target = $atts['target'];
	$url = $atts['url'];

	$link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$color.' is-'.$size.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode( 'button', 'button_shortcode' );

/*----------------------------------------------------------------*\
	VIDEO EMBED
\*----------------------------------------------------------------*/
// Create Shortcode youtube-video
// Shortcode: [youtube-video video-id=""]
function youtube_video_shortcode($atts) {
	$atts = shortcode_atts(
		array(
			'video-id' => '',
		),
		$atts,
		'youtube_video'
	);
	$videoID = $atts['video-id'];
	$iframe = '<a class="video" data-micromodal-trigger="modal-<?php echo $i; ?>" href="javascript:;"> 
			<img src="//i.ytimg.com/vi/'.$videoID.'/hqdefault.jpg">
			<div class="play-button">
				<svg><use xlink:href="#playbutton"></use></svg>
			</div>
		</a>
		<div class="modal micromodal-slide" id="modal-<?php echo $i; ?>" aria-hidden="true">
		<div class="modal__overlay" tabindex="-1" data-micromodal-close>
			<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
				<div class="videoWrapper">
					<iframe id="video-player" width="560" height="315" src="https://www.youtube.com/embed/'.$videoID.'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>';



	return $iframe;
}
add_shortcode( 'youtube_video', 'youtube_video_shortcode' );

/*----------------------------------------------------------------*\
	WARRANTY EMBED
\*----------------------------------------------------------------*/
// Shortcode: [warranty-statement]
function warranty_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'title' => '',
		),
		$atts,
		'modal'
	);
	$title = $atts['title'];
	$modal = '
		<button data-micromodal-trigger="warranty" class="button is-orange is-normal">'.$title .'</button>
		<div class="modal notification-modal micromodal-slide" id="warranty" aria-hidden="true">
			<div class="modal__overlay" tabindex="-1" data-micromodal-close>
				<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
					<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
					'. do_shortcode($content).'
				</div>
			</div>
		</div>
	';
	return $modal;
}
add_shortcode( 'warranty-statement', 'warranty_shortcode' );
