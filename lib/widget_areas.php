<?php
/*----------------------------------------------------------------*\
	INITIALIZE WIDGET AREA
\*----------------------------------------------------------------*/
function custom_sidebars() {
	$args = array(
		'name'          => __( 'Business Details', 'textdomain' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<b>',
		'after_title'   => '</b>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Menus', 'textdomain' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<b>',
		'after_title'   => '</b>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'custom_sidebars' );
