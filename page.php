<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php get_template_part('template-parts/sections/acf-article'); ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>