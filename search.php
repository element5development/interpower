<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<header class="post-header">
	<div>
		<h1>Search Results</h1>
		<hr>
		<?php if (have_posts()) : ?>
			<p>Still can't find what your looking for? <button class="search">try another search</button>.</p>
		<?php else : ?>
			<p>We cannot find anything for "<?php echo(get_search_query()); ?>", <button class="search">try another search</button>.</p>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="card-grid standard-cards is-extra-wide columns-3">
				<?php	while ( have_posts() ) : the_post(); ?>
					<a class="card" href="<?php the_permalink(); ?>">
						<h2><?php the_title(); ?></h2>
						<?php the_excerpt(); ?>
						<div class="button">View <?php echo get_post_type(); ?></div>
					</a>
				<?php endwhile; ?>
			</section>
			<?php clean_pagination(); ?>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>