<?php 
/*----------------------------------------------------------------*\

	SEARCH FORM
	This form is most commonly referanced in the navigation.php file.

\*----------------------------------------------------------------*/
?>

<form role="search" method="get" id="searchform" class="search" action="<?php echo home_url( '/' ); ?>">
	<button type="button" class="close"> 
		<svg><use xlink:href="#close"></use></svg> 
	</button>
  <input type="search" id="s" name="s" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" />
  <button type="submit">Search</button>
</form>