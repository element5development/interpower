<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php $image = get_field('title_background'); ?>
<header class="post-header lazyload" data-expand="250" data-bgset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w" data-sizes="auto">
	<div>
		<h1><?php the_title(); ?></h1>
		<?php the_field('post_description'); ?>
	</div>
</header>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php get_template_part('template-parts/sections/acf-article'); ?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>