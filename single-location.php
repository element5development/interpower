<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-header has-no-image lazyload">
	<div>
		<h1><?php the_title(); ?></h1>
		<?php the_field('post_description'); ?>
		<?php $image = get_field('title_background'); ?>
		<?php if ( get_field('title_background') ) : ?>
			<figure>
				<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</figure>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
	<article>
		<?php get_template_part('template-parts/sections/acf-article'); ?>
	</article>
	<?php else : ?>
	<article>
		<section class="is-narrow">
			<h2>Uh Oh. Something is missing.</h2>
			<p>Looks like this page has no content.</p>
		</section>
	</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>