<?php 
/*----------------------------------------------------------------*\

	COOKIE USE NOTIFICATION BAR
	cookies used must be cleared via WPengine support

\*----------------------------------------------------------------*/
?>
<div class="mobile-call-notification is-active">
	<div> 
	<p>Speak with a sales engineer today.</p>
	<a target="_self" href="tel:+18107989201" class="button is-black is-normal" rel="noopener">+1 (810) 798-9201</a>
	</div>
</div>