<?php 
/*----------------------------------------------------------------*\

	MOBILE PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>

<nav class="primary-navigation is-mobile">
	<a href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/InterpowerLogo.svg" alt="Interpower Logo" />
	</a>
	<div class="right-side">
		<button class="menu">Menu</button>
	</div>
</nav>

<div class="mobile-menu">
	<button class="menu">Close</button>
	<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
	<?php wp_nav_menu(array( 'theme_location' => 'utility_navigation' )); ?>
</div>

<?php if ( get_field('site_notification', 'option') ): ?>
	<div class="site-notification is-mobile is-active">
		<button> 
			<svg><use xlink:href="#close"></use></svg> 
		</button>
		<p><?php the_field('site_notification', 'option'); ?></p>
	</div>
<?php endif; ?>