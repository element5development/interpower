<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>

<div class="utility-navigation">
	<a href="tel:+18107989201">
		Call Now: +1 (810) 798-9201
	</a>
	<nav>
		<?php wp_nav_menu(array( 'theme_location' => 'utility_navigation' )); ?>
	</nav>
</div>

<nav class="primary-navigation">
	<a href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/InterpowerLogo.svg" alt="Interpower Logo" />
	</a>
	<div class="right-side">
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
		<button class="search"> 
			<svg><use xlink:href="#search"></use></svg> 
		</button>
		<?php echo get_search_form(); ?>
	</div>
</nav>

<?php if ( get_field('site_notification', 'option') ): ?>
	<div class="site-notification is-active">
		<button> 
			<svg><use xlink:href="#close"></use></svg> 
		</button>
		<p><?php the_field('site_notification', 'option'); ?></p>
	</div>
<?php endif; ?>