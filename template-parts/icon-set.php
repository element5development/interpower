<?php 
/*----------------------------------------------------------------*\

	SVG ICONS
	List of all the svg icons used throughout the site.
	Allows for easy access without repeat code.

\*----------------------------------------------------------------*/
?>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none">
	<symbol id="email" viewBox="0 0 64 64">
		<path data-name="layer2" fill="none" stroke-miterlimit="10" stroke-width="2" d="M2 12l30 27.4L62 12" stroke-linejoin="round" stroke-linecap="round"/>
		<path data-name="layer1" fill="none" stroke-miterlimit="10" stroke-width="2" d="M2 12h60v40H2z" stroke-linejoin="round" stroke-linecap="round"/>
	</symbol>
	<symbol id="phone" viewBox="0 0 64 64">
		<path data-name="layer1" d="M58.9 47l-10.4-6.8a4.8 4.8 0 0 0-6.5 1.3c-2.4 2.9-5.3 7.7-16.2-3.2S19.6 24.4 22.5 22a4.8 4.8 0 0 0 1.3-6.5L17 5.1c-.9-1.3-2.1-3.4-4.9-3S2 6.6 2 15.6s7.1 20 16.8 29.7S39.5 62 48.4 62s13.2-8 13.5-10-1.7-4.1-3-5z" fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"/>
	</symbol>
	<symbol id="close" viewBox="0 0 48 48">
		<path d="M42 3H6a3 3 0 00-3 3v36a3 3 0 003 3h36a3 3 0 003-3V6a3 3 0 00-3-3zm-8.1 28.071L31.071 33.9 24 26.828 16.929 33.9 14.1 31.071 21.172 24 14.1 16.929l2.829-2.829L24 21.172l7.071-7.072 2.829 2.829L26.828 24z"/>
	</symbol>
	<symbol id="playbutton" viewBox="0 0 48 48">
		<path d="M12.6 5.2A1 1 0 0011 6v36a1 1 0 001.6.8l24-18a.999.999 0 000-1.6l-24-18z"/>
	</symbol>
	<symbol id="search" viewBox="0 0 24 24">
		<path d="M24 22.15A1.88 1.88 0 0122.15 24a1.67 1.67 0 01-1.29-.55l-4.95-4.93a9.9 9.9 0 01-5.76 1.79 10 10 0 01-3.94-.8A10.23 10.23 0 013 17.34 10.16 10.16 0 01.8 14.1a10.13 10.13 0 010-7.89A9.88 9.88 0 016.21.8a10.13 10.13 0 017.89 0A10.16 10.16 0 0117.34 3a10.23 10.23 0 012.17 3.25 10 10 0 01.8 3.94 9.9 9.9 0 01-1.79 5.76l4.95 4.95a1.76 1.76 0 01.53 1.25zm-7.38-12a6.22 6.22 0 00-1.9-4.56 6.22 6.22 0 00-4.57-1.9 6.18 6.18 0 00-4.56 1.9 6.18 6.18 0 00-1.9 4.56 6.22 6.22 0 001.9 4.57 6.22 6.22 0 004.56 1.9 6.26 6.26 0 004.57-1.9 6.26 6.26 0 001.9-4.57z" />
	</symbol>
</svg>