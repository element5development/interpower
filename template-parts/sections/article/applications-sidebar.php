<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing related applications with a left sidebar

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="applications-sidebar <?php the_sub_field('width'); ?>">
	<div class="header">
		<h2><?php the_sub_field('headline'); ?></h2>
	</div>
	<div class="application-section card-grid standard-cards columns-2">
		<?php $applications = get_sub_field('applications'); ?>
		<?php if( $applications ): ?>
			<?php foreach( $applications as $application ): 
				$permalink = get_permalink( $application->ID );
				$title = get_the_title( $application->ID );
				?>
				<a class="card" href="<?php echo esc_url( $permalink ); ?>">
					<div class="card">
						<?php if ( get_field('title_background', $application->ID) ) : ?>
							<?php $image = get_field('title_background', $application->ID); ?>
							<figure>
								<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
							</figure>
						<?php else : ?>
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/no-set-image.PNG" alt="<?php echo $image['alt']; ?>">
							</figure>
						<?php endif; ?>
						<h2><?php echo esc_html( $title ); ?></h2>
						<?php the_field('post_description', $application->ID); ?>
						<div class="button">
							Learn More
						</div>
					</div>
				</a>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</section>