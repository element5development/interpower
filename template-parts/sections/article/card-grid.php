<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of cards

\*----------------------------------------------------------------*/
?>

<?php
	$columns = get_sub_field('columns'); 
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="card-grid <?php the_sub_field('format'); ?>-cards <?php the_sub_field('width'); ?> columns-<?php echo $columns; ?>">
	<?php while ( have_rows('cards') ) : the_row(); ?>
		<?php 
			$link = get_sub_field('button'); 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		?>
		<?php if ( get_sub_field('button') ) : ?>
			<a class="card" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
		<?php else : ?>
			<div class="card">
		<?php endif; ?>
			<!-- IMAGE -->
			<?php $image = get_sub_field('image'); ?>
			<?php if ( get_sub_field('image') ) : ?>
				<figure>
					<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				</figure>
			<?php endif; ?>
			<!-- HEADLINE -->
			<?php if ( get_sub_field('title') ) : ?>
				<h2><?php the_sub_field('title') ?></h2>
			<?php endif; ?>
			<!-- DESCRIPTION -->	
			<?php if ( get_sub_field('description') ) : ?>
				<p><?php the_sub_field('description'); ?></p>
			<?php endif; ?>
			<!-- BUTTON -->
			<?php if ( get_sub_field('button') ) : ?>
				<div class="button">
					<?php echo esc_html($link_title); ?>
				</div>
			<?php endif; ?>
		<?php if ( get_sub_field('button') ) : ?>
			</a>
		<?php else : ?>
			</div>
		<?php endif; ?>
	<?php endwhile; ?>
</section>