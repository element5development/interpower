<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a single wysiwyg editor with a left sidebar

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="editor-sidebar <?php the_sub_field('width'); ?>">
	<div class="header">
		<h2><?php the_sub_field('headline'); ?></h2>
		<?php if( have_rows('links') ): ?>
			<ul>
				<?php while ( have_rows('links') ) : the_row(); ?>
					<?php 
						$link = get_sub_field('link');
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
	<div class="contents">
		<?php the_sub_field('content'); ?>
	</div>
</section>