<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a single wysiwyg editor

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="editor <?php the_sub_field('width'); ?>">
	<?php the_sub_field('content'); ?>
</section>