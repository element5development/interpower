<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying gallery of images

\*----------------------------------------------------------------*/
?>

<?php //GALLERY
	$images = get_sub_field('gallery');

	if ( count($images) > 10 ) :
		$galleryClass = 'large-gallery';
	endif;
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="gallery <?php echo $galleryClass; ?> is-extra-wide">
	<div class="featured">
		<?php $first = true; foreach( $images as $image ): ?>
			<?php if ( $first ) : ?>
				<figure>
					<img id="featuredImage" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
					<?php if ( $image['caption'] ) : ?>
						<p><?php echo esc_html($image['caption']); ?></p>
					<?php endif; ?>
				</figure>
				<?php $first = false; ?>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
	<div class="images">
		<?php foreach( $images as $image ): ?>
			<button>
				<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['caption']; ?>">
			</button>
		<?php endforeach; ?>
	</div>
</section>
