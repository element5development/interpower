<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying newsletter form with headline

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="newsletter">
	<div>
		<div class="headline">
			<h2>Have the future delivered to your inbox every month.</h2>
			<p class="subheader">Sign up for the Interpower Newsletter</p>
		</div>
		<div class="form">
			<?php echo do_shortcode('[gravityform id="5" title="false" description="false"]'); ?>
		</div>
	</div>
</section>