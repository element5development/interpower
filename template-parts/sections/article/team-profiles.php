<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying team member profiles

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="editor-sidebar team-profiles <?php the_sub_field('width'); ?>">
	<div class="header">
		<h2><?php the_sub_field('headline'); ?></h2>
		<?php if( have_rows('links') ): ?>
			<ul>
				<?php while ( have_rows('links') ) : the_row(); ?>
					<?php 
						$link = get_sub_field('link');
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
	<div class="contents">
		<?php while ( have_rows('profile-sections') ) : the_row(); ?>
			<h3><?php the_sub_field('row_header'); ?></h3>
			<div class="team">
				<?php while ( have_rows('profiles') ) : the_row(); ?>
					<div class="member">
						<?php $image = get_sub_field('headshot'); ?>
						<figure>
							<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						</figure>
						<h4><?php the_sub_field('name'); ?></h4>
						<p><?php the_sub_field('job_title'); ?></p>
						<p>
							<?php 
								$phone = preg_replace('/[^0-9]/', '', get_sub_field('phone'));
								$extension = preg_replace('/[^0-9]/', '', get_sub_field('phone_extension'));
							?>
							<a href="tel:<?php echo $phone_link; ?>,<?php echo $extension; ?>"><?php the_sub_field('phone'); ?> <?php the_sub_field('phone_extension') ?></a>
						</p>
						<p>
							<a href="mailto:<?php the_sub_field('email'); ?>">Email</a>
						</p>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endwhile; ?>
	</div>
</section>