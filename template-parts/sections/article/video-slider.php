<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 3 column slider of videos

\*----------------------------------------------------------------*/
?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="video-slider">
	<h2><?php the_sub_field('slider_title'); ?></h2>
	<div class="videos">
		<?php $i=0; while ( have_rows('videos') ) : the_row(); $i++; ?>
			<a class="video" data-micromodal-trigger="modal-<?php echo $i; ?>" href="javascript:;"> 
				<img src="//i.ytimg.com/vi/<?php the_sub_field('video_id'); ?>/hqdefault.jpg">
				<div class="play-button">
					<svg><use xlink:href="#playbutton"></use></svg>
				</div>
			</a>
		<?php endwhile; ?>
	</div>
</section>
<?php $i=0; while ( have_rows('videos') ) : the_row(); $i++; ?>
	<div class="modal micromodal-slide" id="modal-<?php echo $i; ?>" aria-hidden="true">
		<div class="modal__overlay" tabindex="-1" data-micromodal-close>
			<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
				<div class="videoWrapper">
					<iframe id="video-player" width="560" height="315" src="https://www.youtube.com/embed/<?php the_sub_field('video_id'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
<?php endwhile; ?>