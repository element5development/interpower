<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<?php $image = get_field('title_background'); ?>
<header class="post-header lazyload" data-expand="250" data-bgset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w" data-sizes="auto">
	<div>
		<h1><?php the_title(); ?></h1>
		<?php the_field('post_description'); ?>
	</div>
</header>