<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 3 related posts

\*----------------------------------------------------------------*/
?>

<?php $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) ); ?>
<?php if( $related ) : ?>
	<h2 class="is-extra-wide">Related Posts</h2>
	<section class="card-grid standard-cards is-extra-wide columns-3">
		<?php foreach( $related as $post ) : setup_postdata($post); ?>
			<a class="card" href="<?php the_permalink(); ?>">
				<div class="card">
					<?php if ( get_field('title_background') ) : ?>
						<?php $image = get_field('title_background'); ?>
						<figure>
							<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
						</figure>
					<?php else : ?>
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/no-set-image.PNG" alt="<?php echo $image['alt']; ?>">
						</figure>
					<?php endif; ?>
					<h2><?php the_title(); ?></h2>
					<?php if ( get_field('post_description') ) : ?>
						<?php the_field('post_description'); ?>
					<?php endif; ?>
					<div class="button">Continue Reading</div>
				</div>
			</a>
		<?php endforeach; ?>
		<?php wp_reset_postdata(); ?>
	</section>
<?php endif; ?>