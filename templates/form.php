<?php 
/*----------------------------------------------------------------*\

	Template Name: Form
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php $image = get_field('title_background'); ?>
<header class="post-header lazyload" data-expand="250" data-bgset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w" data-sizes="auto">
	<div>
		<h1><?php the_title(); ?></h1>
		<hr>
		<?php the_field('post_description'); ?>
	</div>
</header>

<main id="main-content">
	<article>	
		<section class="form-container">
			<?php the_field('form_content'); ?>
		</section>
		<?php if( have_rows('article') ):  ?>
			<?php get_template_part('template-parts/sections/acf-article'); ?>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>