<?php 
/*----------------------------------------------------------------*\

	Template Name: Home
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>
		<section class="video-highlight is-extra-wide">
			<?php $i=0; while ( have_rows('video_highlights') ) : the_row(); $i++; ?>
				<a data-micromodal-trigger="modal-<?php echo $i; ?>" href="javascript:;"> 
					<div class="video">
						<div>
							<img src="//i.ytimg.com/vi/<?php the_sub_field('video_id'); ?>/hqdefault.jpg">
							<button class="play-button">
								<svg><use xlink:href="#playbutton"></use></svg>
							</button>
						</div>
					</div>
					<h2><?php the_sub_field('video_title'); ?></h2>
				</a>
			<?php endwhile; ?>
			<?php $i=0; while ( have_rows('video_highlights') ) : the_row(); $i++; ?>
				<div class="modal micromodal-slide" id="modal-<?php echo $i; ?>" aria-hidden="true">
					<div class="modal__overlay" tabindex="-1" data-micromodal-close>
						<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
							<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
							<div class="videoWrapper">
								<iframe id="video-player" width="560" height="315" src="https://www.youtube.com/embed/<?php the_sub_field('video_id'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</section>
		<?php if( have_rows('article') ):  ?>
			<?php get_template_part('template-parts/sections/acf-article'); ?>
		<?php endif; ?>
		<?php $posts = get_field('product_cards'); ?>
		<?php if( $posts ): ?>
			<section class="editor-sidebar is-extra-wide">
				<div class="header">
					<?php the_field('product_content'); ?>
				</div>
				<div class="contents card-grid standard-cards columns-2">
					<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
						<a class="card" href="<?php the_permalink(); ?>">
							<div class="card">
								<?php if ( get_field('title_background') ) : ?>
									<?php $image = get_field('title_background'); ?>
									<figure>
										<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
									</figure>
								<?php else : ?>
									<figure>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/no-set-image.PNG" alt="<?php echo $image['alt']; ?>">
									</figure>
								<?php endif; ?>
								<h2><?php the_title(); ?></h2>
								<?php the_field('post_description'); ?>
								<div class="button">
									Learn More
								</div>
							</div>
						</a>
					<?php endforeach; wp_reset_postdata(); ?>
				</div>
			</section>
		<?php endif; ?>
		<?php get_template_part('template-parts/sections/article/newsletter'); ?>
		<section class="split-updates">
			<div class="is-extra-wide">
				<div class="blog">
					<h2><?php the_field('blog_title'); ?></h2>
					<div class="card-grid thumb-cards columns-1">
						<?php $posts = get_field('blog_card'); ?>
						<?php foreach( $posts as $post): setup_postdata($post); ?>
							<a class="card" href="<?php the_permalink(); ?>">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon_News.png" />
								</figure>
								<h2><?php the_title(); ?></h2>
								<?php if ( get_field('post_description') ) : ?>
									<?php the_field('post_description'); ?>
								<?php endif; ?>
								<div class="button">Continue Reading</div>
							</a>
						<?php endforeach; wp_reset_postdata(); ?>
					</div>
				</div>
				<div class="interpower-labs">
					<h2><?php the_field('lab_title'); ?></h2>
					<div class="card-grid thumb-cards columns-1">
						<?php $posts = get_field('lab_card'); ?>
						<?php foreach( $posts as $post): setup_postdata($post); ?>
							<a class="card" href="<?php the_permalink(); ?>">
								<?php $image = get_field('title_background'); ?>
									<figure>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon_Labs.png" />
									</figure>
								<h2><?php the_title(); ?></h2>
								<?php if ( get_field('post_description') ) : ?>
									<?php the_field('post_description'); ?>
								<?php endif; ?>
								<div class="button">Continue Reading</div>
							</a>
						<?php endforeach; wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>